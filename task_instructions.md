#Workshop Task 2#
## Part 1 ##
Bob has installed 5 sensors in the 5 rooms of his 2 floor house. These
are the mac addresses of the 5 sensors:

* ####Floor 1:####
    * **Room 1 sensor mac:** 'r1'
    * **Room 2 sensor mac:** 'r2'

* ####Floor 2:####
    * **Room 3 sensor mac:** 'r3'
    * **Room 4 sensor mac:** 'r4'
    * **Room 5 sensor mac:** 'r5'

Each sensor has a temperature channel that records temperature every 60
seconds in Celsius degrees. 

You are the administrator of a sd_store server and Bob wants to use it.

Bob wants to have a user in the sd_store server with ```username: 'Bob'``` 
and ```password: 123``` so that he can log in. 

Bob wants you to code some functions that use sd_store API to cover the 
following:

* Login
* Sensor Creation
* Channel addition to sensor

Think about which should be the parameters and the outcomes (if any) 
for each of these functions so that Bob can easily use them. 

When you have finished those functions use them to create all Bob's sensors 
and channels. Remember to do all these actions being authenticated as Bob.
(You will have to create Bob's user through Django Admin view)    

## Part 2 ##
Now Bob asks you to create a function that returns the average temperature
of an specific floor only taking into account the last recorded 
temperature in all the rooms of that floor. Bob tells you that if there
were some extra functions to create sensor groups and add sensors to groups 
would also help with this task. 

Once you finish this functionality then create some fake data through
Django Admin view, and try if the average temperature is returned 
successfully.

