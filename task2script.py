import requests
from datetime import datetime, timedelta
import simplejson as json

sd_store_url = 'http://127.0.0.1:8000/'

login_url = sd_store_url + 'sdstore/login/'


def login(usr, password):
    s = requests.Session()
    login_params = {
        'username': usr,
        'password': password
    }
    s.post(login_url, data=login_params)
    return s


def create_sensor(s, mac, name, s_type):
    sensors_url = sd_store_url + 'sdstore/sensors/'
    sensor_params = {"mac": mac,
                     "name": name,
                     "sensor_type": s_type}
    sensor_id = s.post(sensors_url, data=sensor_params)
    print sensor_id.text
    return sensor_id.text


def create_channel(s, mac, name, unit, freq):
    channel_params = {
        "unit": unit,
        "reading_frequency": freq
    }

    channel_url = sd_store_url + 'sdstore/sensor/%s/%s/' % (mac, name)
    r = s.post(channel_url, data=channel_params)
    print r.text


def create_group(s, name, description):
    group_params = {
        "name": name,
        "description": description
    }

    group_url = sd_store_url + 'sdstore/sensorGroups/'
    group_id = s.post(group_url, data=group_params)
    return group_id.text


def add_sensor_group(s, group_id, s_id):
    sensor_params = {'sensorID': s_id}
    group_url = sd_store_url + 'sdstore/sensorGroup/' + str(group_id) + '/sensors/'
    s.post(group_url, data=sensor_params)


def populate_bob_house():
    s = login('Bob', 123)
    macs = ['r1', 'r2', 'r3', 'r4', 'r5']
    s1 = create_sensor(s, macs[0], 'room_1', 'temperature_sensor')
    s2 = create_sensor(s, macs[1], 'room_2', 'temperature_sensor')
    s3 = create_sensor(s, macs[2], 'room_3', 'temperature_sensor')
    s4 = create_sensor(s, macs[3], 'room_4', 'temperature_sensor')
    s5 = create_sensor(s, macs[4], 'room_5', 'temperature_sensor')
    create_channel(s, macs[0], 'ch_1', 'degree C', 60)
    create_channel(s, macs[1], 'ch_1', 'degree C', 60)
    create_channel(s, macs[2], 'ch_1', 'degree C', 60)
    create_channel(s, macs[3], 'ch_1', 'degree C', 60)
    create_channel(s, macs[4], 'ch_1', 'degree C', 60)
    g1 = create_group(s, 'floor1', 'The first floor of Bobs house')
    g2 = create_group(s, 'floor2', 'The second floor of Bobs house')
    add_sensor_group(s, json.loads(g1)['id'], s1)
    add_sensor_group(s, json.loads(g1)['id'], s2)
    add_sensor_group(s, json.loads(g2)['id'], s3)
    add_sensor_group(s, json.loads(g2)['id'], s4)
    add_sensor_group(s, json.loads(g2)['id'], s5)


# populate_bob_house()
'''
s = login('Bob', 123)
gurl = sd_store_url + 'sdstore/sensorGroups/'
g= s.get(gurl)
print g.text
'''


def group_average(s, group_id, channel_name):
    group_url = sd_store_url + 'sdstore/sensorGroup/'+str(group_id)
    group = s.get(group_url)
    sensor_ids = []
    for sensor in json.loads(group.text)['sensors']:
        sensor_ids.append(sensor['id'])

    values = []
    for id in sensor_ids:
        channel_url = sd_store_url + 'sdstore/sensor/%d/%s/last-reading/' % (id, channel_name)
        last = s.get(channel_url)
        values.append(json.loads(last.text)['value'])

    return round(sum(values)*1.0/len(values))

s = login('Bob', 123)
print group_average(s, 1, 'ch_1')
